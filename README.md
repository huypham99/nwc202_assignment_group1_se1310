# NWC202_Assignment_Group1_SE1310

**This is the assignment of NWC202 - Computer Networking Course at FPT University**

**Some important notes:**
*  This project is about a simple chat application which could be deployed in computers those **connect** to a **same network**.
*  The project has 2 main parts: **Chat application** written in Java, and **Network Simulation** run by NS2 (Network Simulator)
1. **Java Chat Application:**
*  Chat application consists of 2 programs: a server and the clients.
*  Server has to be opened so that the client could connect to each others.
*  Link of **tutorial-video** (how to use not how to code): [Click here](https://www.youtube.com/watch?v=wI__EYZOLfc&feature=youtu.be)
2. **Network Simulation (Giả Lập Mạng)**
*  Your lecturer would ask you to install **ns2** on ubuntu (could use virtual machine), if **not**, this part was **not required in your course assignment**.
*  Follow these steps to run NS:
    1. Copy file ***~/Network Simulation/init.tcl*** to your directory on ubuntu.
    2. Open terminal, use cd to access directory containing ***init.tcl***
    3. Enter **ns init.tcl** => Then your directory would contain these files: ***init.nam***, ***init.tr***, ***graph.xg*** and ***init.tcl*** (old file).
    4. Enter **nam init.nam** in terminal => NAM (Network Animator) would run, then click on play button to see the network simulation.
    5. Enter **xgraph graph.xg -geometry 800x400** => then you will see the graph of ack value.
    
You could read ***~/Report document.docx*** and ***~/NWC.pptx*** for more information.

**By Pham Quang Huy**